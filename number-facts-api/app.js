let fact = document.querySelector("#fact");
let factText = document.querySelector("#factText");
let numberInput = document.querySelector("#numberInput");
let choose = document.querySelector("#choose");

choose.addEventListener("change", getChoose);
numberInput.addEventListener("input", getFact);

function getChoose(e) {
  let choose = e.value;
  getFact();
}

function getFact() {
  let number = numberInput.value;
  fetch(`http://numbersapi.com/${number}/${choose.value === 'number' ? '' : choose.value }`)
    .then((response) => response.text())
    .then((data) => {
      if (number != "") {
        fact.style.display = "block";
        factText.textContent = data;
      } else {
        fact.style.display = "none";
      }
    });
}
