// get modal element
const modal = document.querySelector("#simpleModal");
// get open modal btn
const modalBtn = document.querySelector("#modalBtn");
// get close btn
const closeBtn = document.querySelector(".closeBtn");

// listen for open modal
modalBtn.addEventListener("click", openModal);

// listen for close modal
closeBtn.addEventListener("click", closeModal);

// listen for outside of modal click to close the modal
window.addEventListener("click", clickOutside);

function openModal() {
  modal.style.display = "block";
}

function closeModal() {
  modal.style.display = "none";
}

function clickOutside(e) {
  if (e.target === modal) {
    modal.style.display = "none";
  }
}
