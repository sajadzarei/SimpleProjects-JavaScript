// get value of select input
document.getElementById("choose").addEventListener("change", function (e) {
  let choose = e.target.value;
});

// hide select input and its label 
document.getElementById("choose").previousElementSibling.style.visibility ="visible";
document.getElementById("choose").style.visibility = "visible";

// hide cards when page loads
document.getElementById("output").style.visibility = "hidden";

// get value of input when use types in 
document.getElementById("lbsInput").addEventListener("input", function (e) {
  // show cards when typing   
  document.getElementById("output").style.visibility = "visible";
  let lbs = e.target.value;
  // change select input label text  
  document.getElementById(
    "choose"
  ).previousElementSibling.innerHTML = `<h5>Your converting from "${choose.value}"</h5>`;
  // hide select input element   
  document.getElementById("choose").style.visibility = "hidden";
  
  // check if user select pounds
  if (choose.value === "pounds") {
    // convert to grams
    document.getElementById("gramsOutput").innerHTML = lbs / 0.0022046;
    // convert to Kg
    document.getElementById("kgOutput").innerHTML = lbs / 2.205;
    // convert to Oz
    document.getElementById("ozOutput").innerHTML = lbs * 16;
  }
  // check if user select kilograms
  if (choose.value === "kilograms") {
    // convert to grams
    document.getElementById("gramsOutput").innerHTML = lbs / 1000;
    // convert to Pounds
    document.getElementById("kgOutput").previousElementSibling.textContent =
      "Pounds:";
    document.getElementById("kgOutput").innerHTML = lbs * 2.205;
    // convert to Oz
    document.getElementById("ozOutput").innerHTML = lbs * 35.274;
  }
  // check if user select grams
  if (choose.value === "grams") {
    // convert to pounds
    document.getElementById("gramsOutput").previousElementSibling.textContent =
      "Pounds:";
    document.getElementById("gramsOutput").innerHTML = lbs / 454;
    // convert to Kg
    document.getElementById("kgOutput").innerHTML = lbs / 1000;
    // convert to Oz
    document.getElementById("ozOutput").innerHTML = lbs / 28.35;
  }
  // check if user select ounces
  if (choose.value === "ounces") {
    // convert to gr
    document.getElementById("gramsOutput").innerHTML = lbs * 28.35;
    // convert to Kg
    document.getElementById("kgOutput").innerHTML = lbs / 35.274;
    // convert to pounds
    document.getElementById("ozOutput").previousElementSibling.textContent =
      "Pounds:";
    document.getElementById("ozOutput").innerHTML = lbs / 16;
  }
});
