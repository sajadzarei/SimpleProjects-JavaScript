document.querySelector("#myForm").addEventListener("submit", saveBookmark);

function saveBookmark(e) {
  const siteName = document.querySelector("#siteName").value;
  const siteUrl = document.querySelector("#siteUrl").value;

  if (!validateForm(siteName, siteUrl)) {
    return false;
  }
  const bookmark = {
    name: siteName,
    url: siteUrl,
  };

  if (localStorage.getItem("bookmarks") === null) {
    const bookmarks = [];
    bookmarks.push(bookmark);
    localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
  } else {
    const bookmarks = JSON.parse(localStorage.getItem("bookmarks"));
    bookmarks.push(bookmark);
    localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
  }
  document.querySelector("#myForm").reset();
  fetchBookmarks();
  e.preventDefault();
}

function deleteBookmark(url) {
  console.log(url);
  const bookmarks = JSON.parse(localStorage.getItem("bookmarks"));
  for (let i = 0; i < bookmarks.length; i++) {
    if (bookmarks[i].url === url) {
      bookmarks.splice(i, 1);
    }
  }
  localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
  fetchBookmarks();
}

function fetchBookmarks() {
  const bookmarks = JSON.parse(localStorage.getItem("bookmarks"));
  const bookmarksResults = document.querySelector("#bookmarksResults");
  bookmarksResults.innerHTML = "";
  for (let i = 0; i < bookmarks.length; i++) {
    const name = bookmarks[i].name;
    const url = bookmarks[i].url;
    bookmarksResults.innerHTML += `
      <div class="bg-light p-3 mb-2">
      <h3>${name} <br>
      <a class="btn btn-secondary mt-3" target="_blank" href="${url}">Visit</a>
      <a onclick="deleteBookmark('${url}')" class="btn btn-outline-danger mt-3" href="#">Delete</a>
      </h3>
      </div>`;
  }
}

function validateForm(siteName, siteUrl) {
  if (!siteName || !siteUrl) {
    alert("Please fill in the form!");
    return false;
  }
  const expression =
    /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
  const regex = new RegExp(expression);

  if (!siteUrl.match(regex)) {
    alert("Please use a valid url!");
    return false;
  }
  return true;
}
